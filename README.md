# sccm-ADR-office-updates

Powershell Script, das über die Statusfilter Regeln von SCCM automatisiert aufgerufen wird und auf die Erstellung einer Softwareupdategruppe(SUG) reagiert.

Zerlegt die gesamten Updates in die verschiedenen Office Channels und stellt sie als getrennte SUG bereit.